//
//  AppDelegate.swift
//  rc
//
//  Created by Artem Hruzd on 9/15/17.
//  Copyright © 2017 Artem Hruzd. All rights reserved.
//

import Cocoa
import RealmConverter
import Realm

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        let argLen = CommandLine.arguments.count
        var inputPath = ""
        var outputPath = ""
        
        for (i, argument) in CommandLine.arguments.enumerated() {
            switch argument {
            case "--input":
                if (argLen > i+1) {
                    inputPath = CommandLine.arguments[i+1]
                }
            case "--output":
                if (argLen > i+1) {
                    outputPath = CommandLine.arguments[i+1]
                }
            default:
                break;
            }
            
        }
        
        if (inputPath != "" && outputPath != "") {
            do {
                if let urlInputPath = URL(string: inputPath) {
                    print("Reading .realm file: \(urlInputPath)")
                    let configuration = RLMRealmConfiguration.default()
                    configuration.fileURL = urlInputPath
                    configuration.dynamic = true
                    
                    let realm = try RLMRealm(configuration: configuration)
                    let csvDataExporter = CSVDataExporter(realm: realm)
                    print("Exporting .csv files to: \(outputPath)")
                    try csvDataExporter.export(toFolderAtPath: outputPath)
                    print("Completed")
                } else {
                    print("Coudn't convert path: \(inputPath)")
                }
                
            } catch {
                print(error)
            }
            NSApplication.shared().terminate(self)
        }
        
        
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

