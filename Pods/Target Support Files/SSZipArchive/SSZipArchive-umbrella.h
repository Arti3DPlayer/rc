#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#endif

#import "SSZipArchive.h"
#import "SSZipCommon.h"
#import "ZipArchive.h"

FOUNDATION_EXPORT double SSZipArchiveVersionNumber;
FOUNDATION_EXPORT const unsigned char SSZipArchiveVersionString[];

