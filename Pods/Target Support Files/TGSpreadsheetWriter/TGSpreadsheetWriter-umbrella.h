#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#endif

#import "TGSpreadsheetWriter.h"
#import "Common.h"
#import "SSZipArchive.h"
#import "ZipArchive.h"

FOUNDATION_EXPORT double TGSpreadsheetWriterVersionNumber;
FOUNDATION_EXPORT const unsigned char TGSpreadsheetWriterVersionString[];

