#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
#endif

#import "RealmConverter.h"

FOUNDATION_EXPORT double RealmConverterVersionNumber;
FOUNDATION_EXPORT const unsigned char RealmConverterVersionString[];

